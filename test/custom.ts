import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("Test valid link email!", () => {

        // put the body of the test here
        const e = new EngExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("mail.")
            .beginCapture()
              .beginCapture()
                .anythingBut("/")
              .endCapture()
              .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        const result = e.exec("https://mail.google.com/mail");

        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.
        expect(result[1]).to.be.equal("google.com/mail");
        expect(result[2]).to.be.equal("google.com");
        expect(e.test("https://mail.google.com/mail")).to.be.true;
        expect(result[0]).to.be.equal("https://mail.google.com/mail");
    });

    it("Test valid Yahoo Email address!", () => {

        //  the body of the test
        const e = new EngExp()
            .startOfLine()
            .anythingBut(" ")
            .then("@")
            .beginCapture()
              .beginCapture()
              .anythingBut(" ")
              .endCapture()
              .then(".com")
            .endCapture()
            .endOfLine()
            .asRegExp();
        const result = e.exec("canada1975@yahoo.com");
        expect(result[1]).to.be.equal("yahoo.com");
        expect(e.test("usd1975@gmail.com")).to.be.true;
        expect(result[0]).to.be.equal("canada1975@yahoo.com");
    });

    it("Test valid password!", () => {

      //the body of the test
      const spec = new EngExp()
            .or("@").or("#").or("$")
            .or("%").or("^").or("&").or("*")
            .or("()").or(")").or("<").or(">").or("?");
      const e = new EngExp()
      .startOfLine()
      .anythingBut(" ")
      .then("#").or("!")
        .beginCapture()
        .anythingBut(" ")
        
     .endCapture()
      .endOfLine()
      .asRegExp();
  expect(e.test("baicaconca!12345con!")).to.be.true;
  expect(e.test("%baicaconca#12345con")).to.be.true;
  });

  it("Test valid date", () => {
    const month = new EngExp()
        .match("Jan").or("Feb").or("Mar").or("Apr")
        .or("May").or("Jun").or("Jul").or("Aug")
        .or("Sep").or("Oct").or("Nov").or("Dec");
    const e = new EngExp()
        .startOfLine().beginLevel()
        .digit().repeated(1, 2)
        .then("/")
        .then(new EngExp().digit().repeated(1, 2))
        .then("/")
        .then(new EngExp().digit().repeated(2, 4))
        .or(
         new EngExp()
            .digit().repeated(1, 2)
            .then(" ")
            .then(month)
            .then(" ")
            .then(new EngExp().digit().repeated(2, 4))
        )
        .or(
          new EngExp()
             .digit().repeated(1, 2)
             .then("/")
             .then(month)
             .then("/")
             .then(new EngExp().digit().repeated(2, 4))
         )
        .endLevel().endOfLine()
        .asRegExp();
    expect(e.test("12/25/2015")).to.be.true;
    expect(e.test("25/Dec/2015")).to.be.true;
    expect(e.test("77/77/77/77")).to.be.false;
    expect(e.test("999 Dec 9999")).to.be.false;
    expect(e.test("25 Dec 2015")).to.be.true;
});
it("Search a string", () => {
  const e = new EngExp()
      .startOfLine().beginLevel()
      .then("Toi")
      .maybe(" ")
      .then("di")
      .maybe(" ")
      .maybe("choi")
      .maybe(" ")
      .then("ma")
      .maybe(" ")
      .maybe("khong")
      .maybe(" ")
      .maybe("ai")
      .maybe(" ")
      .maybe("kiem")
      .maybe(" ")
      .maybe("toi")
      .beginCapture()
        .beginCapture()
          .anythingBut("ma")
        .endCapture()
        .anythingBut(" ")
      .endCapture()
      .endLevel().endOfLine()
      .asRegExp();
  const result = e.exec("Toi di choi ma khong ai kiem toi");
  expect(e.test("Toi di choi ma khong ai kiem toi")).to.be.true;
  });
  
});
